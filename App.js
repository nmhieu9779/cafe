import React, { Component } from "react"
import { View, Text } from "react-native"
import Tabbar from "react-native-tabbar-bottom"
import LoginScreen from "./src/Login/LoginScreen"
import HomeScreen from "./src/Home/HomeScreen"
import MenuScreen from "./src/MenuScreen/MenuScreen"
import { saveDataMenu } from "./src/Data/DataMenu"
export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loginSuccess: false,
      page: "HomeScreen"
    }
  }
  componentWillMount = () => {
    // saveDataMenu()
    // validateInfoLogin()
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        {!this.state.loginSuccess ? (
          <LoginScreen
            onLogin={status => {
              this.setState({ loginSuccess: status })
            }}
          />
        ) : (
          <View style={{ flex: 1 }}>
            {this.state.page === "HomeScreen" && <HomeScreen />}
            {this.state.page === "MenuScreen" && <MenuScreen />}
            <Tabbar
              stateFunc={tab => {
                this.setState({ page: tab.page })
              }}
              activePage={this.state.page}
              tabs={[
                {
                  page: "HomeScreen",
                  icon: "home"
                },
                {
                  page: "MenuScreen",
                  icon: "menu"
                }
              ]}
            />
          </View>
        )}
      </View>
    )
  }
}
