import React, { Component } from "react"
import { FlatList, View, ImageBackground } from "react-native"
import Table from "../Component/Table"
import { getAccessToken } from "../Data/Login"
export default class TableScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      refreshing: false
    }
  }
  componentDidMount = () => {
    getAccessToken()
      .then(token => JSON.parse(token).assessToken)
      .then(assessToken => {
        var oauthHeader = "Bearer " + assessToken
        fetch("http://35.247.162.95:32120/api/v1/tables", {
          headers: {
            Authorization: oauthHeader
          }
        }).then(response => {
          response
            .json()
            .then(responseJson =>
              responseJson.map(item => {
                return {
                  id: item.id,
                  totalProduct: item.totalProduct,
                  totalPrice: item.totalPrice
                }
              })
            )
            .then(dataState => {
              this.setState({ data: dataState, refreshing: false })
            })
        })
      })
  }
  _refresh = () => {
    this.setState({ refreshing: true })
    setTimeout(() => {
      this.componentDidMount()
    }, 500)
  }

  render() {
    console.log(this.state)
    return (
      <ImageBackground
        source={require("../../res/Background.jpg")}
        style={{
          width: "100%",
          height: "100%",
          paddingBottom: 50
        }}
      >
        <FlatList
          data={this.state.data}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          numColumns={3}
          refreshing={this.state.refreshing}
          onRefresh={this._refresh.bind(this)}
        />
      </ImageBackground>
    )
  }

  _keyExtractor = item => item.id
  _renderItem = ({ item }) => {
    return (
      <View
        style={{
          height: 150,
          width: "33%",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Table
          idTable={item.id}
          Order={item.totalProduct != 0}
          onPress={() => {
            this.props.navigation.navigate("DetailScreen", {
              data: item.id,
              totalPrice: item.totalPrice
            })
          }}
        />
      </View>
    )
  }
}
