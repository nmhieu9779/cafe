import React, { Component } from "react"
import {
  FlatList,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Alert
} from "react-native"
import Icon from "react-native-vector-icons/FontAwesome"
import { getAccessToken } from "../Data/Login"

export default class DetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idTable: null,
      Order: [],
      refresh: false,
      totalPrice: 0
    }
  }
  componentDidMount = () => {
    const { navigation } = this.props
    const idTable = navigation.getParam("data")
    const totalPrice = navigation.getParam("totalPrice")
    this.setState({ totalPrice: totalPrice })
    this.getDataDetail(idTable)
  }
  getDataDetail = idTable => {
    getAccessToken()
      .then(token => JSON.parse(token).assessToken)
      .then(assessToken => {
        var oauthHeader = "Bearer " + assessToken
        fetch("http://35.247.162.95:32120/api/v1/orders/" + idTable, {
          headers: {
            Authorization: oauthHeader
          }
        }).then(response => {
          response
            .json()
            .then(responseJson => {
              return responseJson.map(item => ({
                id: item.id,
                name: item.product.name,
                quantity: item.quantity,
                price: item.totalPrice
              }))
            })
            .then(dataState => {
              this.setState({ idTable: idTable, Order: dataState })
            })
        })
      })
  }
  render() {
    console.log(this.state.Order)
    return (
      <ImageBackground
        source={require("../../res/Background.jpg")}
        style={{
          width: "100%",
          height: "100%",
          paddingBottom: 50
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            backgroundColor: "#89dfde",
            height: 50,
            paddingLeft: 20,
            paddingRight: 20
          }}
        >
          <Text style={{ fontSize: 25 }}>
            <Text style={{ fontWeight: "bold" }}>Name:</Text>
            {this.state.idTable}
          </Text>
          <Text style={{ fontSize: 25 }}>
            <Text style={{ fontWeight: "bold" }}>Total Price:</Text>
            {this.state.totalPrice}
          </Text>
        </View>
        <FlatList
          data={this.state.Order}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={this.state.refresh}
        />
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={{
              width: "100%",
              borderWidth: 1,
              height: 50,
              backgroundColor: "#89dfde",
              borderRadius: 10,
              marginTop: 10
            }}
            onPress={() => {
              Alert.alert(
                "Pay",
                "",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  {
                    text: "OK",
                    onPress: () => {
                      getAccessToken()
                        .then(token => JSON.parse(token).assessToken)
                        .then(assessToken => {
                          var oauthHeader = "Bearer " + assessToken
                          const url =
                            "http://35.247.162.95:32120/api/v1/orders/checkout/" +
                            this.state.idTable
                          let fetchData = {
                            method: "DELETE",
                            headers: {
                              "Content-Type": "application/json",
                              Authorization: oauthHeader
                            }
                          }
                          fetch(url, fetchData)
                            .then(response => {
                              response.ok ? "" : alert("fail")
                            })
                            .then(() => {
                              this.props.navigation.navigate("TableScreen")
                            })
                        })
                    }
                  }
                ],
                { cancelable: false }
              )
            }}
            activeOpacity={0.8}
          >
            <Text
              style={{
                flex: 1,
                textAlign: "center",
                textAlignVertical: "center",
                color: "#000",
                fontSize: 20
              }}
            >
              PAY
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 50,
              width: 50,
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              bottom: 80,
              right: 20,
              backgroundColor: "#89dfde",
              borderRadius: 999
            }}
            activeOpacity={0.8}
            onPress={() => {
              this.props.navigation.navigate("OrderScreen", {
                data: this.state.idTable
              })
            }}
          >
            <Icon name="plus" color="#eee" size={30} />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
  _keyExtractor = (item, index) => item.id + index.toString()
  _renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          borderWidth: 0.5,
          borderColor: "#ccc",
          marginBottom: 10,
          marginTop: 10
        }}
      >
        <Image
          source={require("../../res/hamburger.png")}
          style={{
            width: 70,
            height: 70,
            margin: 10
          }}
        />
        <View style={{ flex: 1, marginLeft: 10 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, color: "#fff" }}>
            {item.name}
          </Text>
          <Text>x{item.quantity}</Text>
          <Text style={{ fontWeight: "bold", color: "red" }}>{item.price}</Text>
        </View>
        <Icon
          style={{ position: "absolute", top: 5, right: 5 }}
          name="trash"
          color="red"
          size={30}
          onPress={() => {
            this.payOrder({ ...item }.id)
          }}
        />
      </View>
    )
  }
  removeOrder = id => {
    data = this.state.Order
    var removeIndex = data.map(item => item.id).indexOf(id)
    data.splice(removeIndex, 1)
    this.setState({ Order: data, refresh: !this.state.refresh })
  }
  payOrder = id => {
    getAccessToken()
      .then(token => JSON.parse(token).assessToken)
      .then(assessToken => {
        var oauthHeader = "Bearer " + assessToken
        const url = "http://35.247.162.95:32120/api/v1/orders/" + id
        let fetchData = {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: oauthHeader
          }
        }
        fetch(url, fetchData).then(response => {
          response.ok ? this.removeOrder(id) : alert("fail")
        })
      })
  }
}
