import React, { Component } from "react"
import {
  FlatList,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Alert
} from "react-native"
import Icon from "react-native-vector-icons/FontAwesome"
import { getAccessToken } from "../Data/Login"

export default class OrderScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      idTable: null,
      data: [],
      change: true
    }
  }
  componentDidMount = () => {
    const { navigation } = this.props
    const idTable = navigation.getParam("data")
    getAccessToken()
      .then(token => JSON.parse(token).assessToken)
      .then(assessToken => {
        var oauthHeader = "Bearer " + assessToken
        fetch("http://35.247.162.95:32120/api/v1/products", {
          headers: {
            Authorization: oauthHeader
          }
        }).then(response => {
          response
            .json()
            .then(responseJson =>
              responseJson.map(item => ({
                id: item.id,
                name: item.name,
                quantity: 0
              }))
            )
            .then(dataState => {
              this.setState({ idTable: idTable, data: dataState })
            })
        })
      })
  }
  render() {
    return (
      <ImageBackground
        source={require("../../res/Background.jpg")}
        style={{
          width: "100%",
          height: "100%",
          paddingBottom: 50
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            backgroundColor: "#89dfde",
            height: 50,
            paddingLeft: 20,
            paddingRight: 20
          }}
        />
        <FlatList
          data={this.state.data}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={this.state.change}
        />
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={{
              width: "100%",
              borderWidth: 1,
              height: 50,
              backgroundColor: "#89dfde",
              borderRadius: 10,
              marginTop: 10
            }}
            // onPress={() => {
            //   navigate("OrderScreen")
            // }}
            activeOpacity={0.8}
          >
            <Text
              style={{
                flex: 1,
                textAlign: "center",
                textAlignVertical: "center",
                color: "#000",
                fontSize: 20
              }}
              onPress={() => {
                var me = this
                getAccessToken()
                  .then(token => JSON.parse(token).assessToken)
                  .then(assessToken => {
                    var oauthHeader = "Bearer " + assessToken
                    const url =
                      "http://35.247.162.95:32120/api/v1/orders/" +
                      this.state.idTable
                    let data = me.state.data
                      .filter(item => item.quantity > 0)
                      .map(item => ({
                        productId: item.id,
                        quantity: item.quantity
                      }))
                    let fetchData = {
                      method: "POST",
                      body: JSON.stringify(data),
                      headers: {
                        "Content-Type": "application/json",
                        Authorization: oauthHeader
                      }
                    }
                    fetch(url, fetchData).then(response => {
                      response.ok
                        ? me.showAlert("Success")
                        : me.showAlert("Fail")
                    })
                  })
              }}
            >
              ORDER
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
  showAlert = text => {
    Alert.alert(text, "", [
      {
        text: "OK",
        onPress: () => this.props.navigation.navigate("TableScreen")
      }
    ])
  }

  _keyExtractor = (item, index) => item.id + index.toString()
  _renderItem = ({ item, index }) => {
    console.log(item)
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          borderWidth: 0.5,
          borderColor: "#ccc",
          marginBottom: 10,
          marginTop: 10
        }}
        activeOpacity={0.9}
      >
        <Image
          source={require("../../res/hamburger.png")}
          style={{
            width: 70,
            height: 70,
            margin: 10
          }}
        />
        <View style={{ flex: 1 }}>
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 20,
              marginBottom: 20,
              textAlign: "center"
            }}
          >
            {item.name}
          </Text>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text>x{item.quantity}</Text>
            <View style={{ flexDirection: "row", marginRight: 20 }}>
              <Icon
                style={{ marginRight: 20 }}
                name="minus"
                color="#eee"
                size={30}
                onPress={() => {
                  var data = this.state.data
                  data[index].quantity--
                  this.setState({ change: !this.state.change, data: data })
                }}
              />
              <Icon
                name="plus"
                color="#eee"
                size={30}
                onPress={() => {
                  var data = this.state.data
                  data[index].quantity++
                  this.setState({ change: !this.state.change, data: data })
                }}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }
}
