export default (getDataTable = () => {
  fetch("http://35.247.162.95:32120/api/v1/tables")
    .then(response => {
      return response.json()
    })
    .then(responseJson => {
      return responseJson.map(item => ({ id: item.id, people: item.people }))
    })
    .catch(error => {
      console.error(error)
    })
})
