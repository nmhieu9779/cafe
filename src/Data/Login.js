import { AsyncStorage } from "react-native"

export const validateInfoLogin = async (userName, passWord) => {
  var basicAuth = "Basic " + btoa("OnlyuClientId:kinlove32")
  fetch("http://35.247.162.95:32107/oauth/token", {
    method: "POST",
    headers: {
      Authorization: basicAuth,
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body:
      "grant_type=password" +
      "&username=" +
      userName +
      "&password=" +
      passWord +
      "&scope=ui"
  })
    .then(response => response.json())
    .then(responseJson => {
      this.saveAccessToken(responseJson.access_token)
    })
}

saveAccessToken = async assessToken => {
  try {
    await AsyncStorage.setItem(
      "assessToken",
      JSON.stringify({ assessToken: assessToken })
    )
  } catch (error) {
    console.log(error.message)
  }
}

export const getAccessToken = async () => {
  console.log("Get Assess_Token")
  let assessToken = ""
  try {
    assessToken =
      (await AsyncStorage.getItem("assessToken")) ||
      JSON.stringify({ assessToken: "" })
  } catch (error) {
    console.log(error.message)
  }
  return assessToken
}
