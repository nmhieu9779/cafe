import React, { Component } from "react"
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground
} from "react-native"
import ImageInput from "../Component/ImageInput"
import { validateInfoLogin, getAccessToken } from "../Data/Login"

export default class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: "admin1",
      passWord: "kinlove32"
    }
  }
  componentDidMount = () => {}

  render() {
    return (
      <ImageBackground
        source={require("../../res/Background.jpg")}
        style={{
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            height: 350,
            width: "80%",
            alignItems: "center"
          }}
        >
          <View style={{ height: 60, width: "100%" }} />
          <View
            style={{
              height: 60,
              width: "100%",
              backgroundColor: "#89dfde",
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            }}
          />
          <Image
            style={{ width: 120, height: 120, position: "absolute", top: 0 }}
            source={require("../../res/Logo.png")}
          />
          <View
            style={{
              flex: 1,
              width: "100%",
              alignItems: "center",
              paddingTop: 20,
              backgroundColor: "#89dfde",
              paddingLeft: 10,
              paddingRight: 10,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20
            }}
          >
            <ImageInput
              UrlImage={require("../../res/UserName.png")}
              Placeholder="UserName"
              Value={this.state.userName}
              SecureTextEntry={false}
              onChangeText={text => {
                this.setState({ userName: text })
              }}
            />
            <ImageInput
              UrlImage={require("../../res/PassWord.png")}
              Placeholder="PassWord"
              Value={this.state.passWord}
              SecureTextEntry={true}
              onChangeText={text => {
                this.setState({ passWord: text })
              }}
            />
            <TouchableOpacity
              style={{
                width: "100%",
                borderWidth: 1,
                height: 50,
                backgroundColor: "#000",
                borderRadius: 10,
                marginTop: 10
              }}
              activeOpacity={0.8}
              onPress={this.onPress.bind(this)}
            >
              <Text
                style={{
                  flex: 1,
                  textAlign: "center",
                  textAlignVertical: "center",
                  color: "#fff",
                  fontSize: 20
                }}
              >
                LOGIN
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }
  onPress = () => {
    const { userName, passWord } = this.state
    validateInfoLogin(userName, passWord).then(() => {
      getAccessToken().then(assessToken => {
        this.props.onLogin(JSON.parse(assessToken).assessToken !== undefined)
      })
    })
  }
}
