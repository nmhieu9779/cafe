import React, { PureComponent } from "react"
import { TouchableOpacity, Image, Text } from "react-native"

export default class TableScreen extends PureComponent {
  render() {
    return (
      <TouchableOpacity
        style={{
          height: 100,
          width: 100,
          borderRadius: 999,
          backgroundColor: "#fff",
          justifyContent: "center",
          alignItems: "center"
        }}
        activeOpacity={0.8}
        onPress={this.props.onPress}
      >
        {this.props.Order ? (
          <Image
            style={{ position: "absolute", top: 10 }}
            source={require("../../res/Order.png")}
          />
        ) : (
          <Image
            style={{ position: "absolute", top: 10 }}
            source={require("../../res/NoOrder.png")}
          />
        )}
        <Text
          style={{
            fontSize: 20,
            fontWeight: "bold",
            backgroundColor: "#ccc",
            width: "100%",
            textAlign: "center",
            borderBottomLeftRadius: 999,
            borderBottomRightRadius: 999,
            position: "absolute",
            bottom: 0,
            height: "50%",
            textAlignVertical: "center"
          }}
        >
          {this.props.idTable}
        </Text>
      </TouchableOpacity>
    )
  }
}
