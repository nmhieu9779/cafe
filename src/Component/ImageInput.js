import React, { PureComponent } from "react"
import { View, Image, TextInput } from "react-native"

export default class ImageInput extends PureComponent {
  render() {
    return (
      <View
        style={{
          width: "100%",
          height: null,
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "white",
          borderRadius: 10,
          marginBottom: 10
        }}
      >
        <Image style={{ margin: 10 }} source={this.props.UrlImage} />
        <TextInput
          style={{
            flex: 1,
            fontSize: 20
          }}
          placeholder={this.props.Placeholder}
          secureTextEntry={this.props.SecureTextEntry}
          onChangeText={this.props.onChangeText}
        >
          {this.props.Value}
        </TextInput>
      </View>
    )
  }
}
