import TableScreen from "../TableScreen/TableScreen"
import DetailScreen from "../TableScreen/DetailScreen"
import OrderScreen from "../TableScreen/OrderScreen"
import { createStackNavigator, createAppContainer } from "react-navigation"

const App = createAppContainer(
  createStackNavigator(
    {
      TableScreen: {
        screen: TableScreen,
        navigationOptions: {
          header: null
        }
      },
      DetailScreen: {
        screen: DetailScreen,
        navigationOptions: {
          header: null
        }
      },
      OrderScreen: {
        screen: OrderScreen,
        navigationOptions: {
          header: null
        }
      }
    },
    { initialRouteName: "TableScreen" }
  )
)

export default App
