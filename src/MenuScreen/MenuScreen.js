import React, { Component } from "react"
import { FlatList, View, Text, ImageBackground, Image } from "react-native"
import { getAccessToken } from "../Data/Login"

export default class DetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = []
  }
  componentDidMount = () => {
    getAccessToken()
      .then(token => JSON.parse(token).assessToken)
      .then(assessToken => {
        var oauthHeader = "Bearer " + assessToken
        fetch("http://35.247.162.95:32120/api/v1/products", {
          headers: {
            Authorization: oauthHeader
          }
        }).then(response => {
          response
            .json()
            .then(responseJson => {
              return responseJson.map(item => ({
                id: item.id,
                name: item.name,
                price: item.price
              }))
            })
            .then(data => {
              this.setState({ data: data })
            })
        })
      })
  }
  render() {
    return (
      <ImageBackground
        source={require("../../res/Background.jpg")}
        style={{
          width: "100%",
          height: "100%",
          paddingBottom: 50
        }}
      >
        <FlatList
          data={this.state.data}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
        />
      </ImageBackground>
    )
  }
  _keyExtractor = (item, index) => item.id + index.toString()
  _renderItem = ({ item }) => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          borderWidth: 0.5,
          borderColor: "#ccc",
          marginBottom: 10,
          marginTop: 10
        }}
      >
        <Image
          source={require("../../res/hamburger.png")}
          style={{
            width: 70,
            height: 70,
            margin: 10
          }}
        />
        <View style={{ flex: 1, marginLeft: 10 }}>
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 20,
              marginBottom: 10,
              color: "#fff"
            }}
          >
            {item.name}
          </Text>
          <Text style={{ fontWeight: "bold", color: "red" }}>
            {item.price} VND
          </Text>
        </View>
      </View>
    )
  }
}
