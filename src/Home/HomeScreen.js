import React, { Component } from "react"
import { View, ImageBackground } from "react-native"
import App from "../Component/Navigation"

export default class HomeScreen extends Component {
  render() {
    return (
      <ImageBackground
        source={require("../../res/Background.jpg")}
        style={{
          width: "100%",
          height: "100%"
        }}
      >
        <App />
      </ImageBackground>
    )
  }
}
